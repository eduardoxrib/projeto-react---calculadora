 
 Projeto desenvolvido como uma introdução de como o framework React funciona.  
 
##  Rodando o Projeto
 
 - Clone o repositório para uma pasta
 - Dentro da pasta do projeto rode o comando: "npm install"
 - Após isso, rode o comando: "npm start"
 - O navegador abrirá automaticamente
 - Caso não abra, coloque o link "localhost:3000" no seu navegador 

